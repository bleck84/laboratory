package com.bleck84.pocker;

import com.bleck84.pocker.data.Card;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashSet;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by bleck84 on 4/1/16.
 */
public class Main {

    private static final Logger LOGGER = LoggerFactory.getLogger(Main.class);

    private static void insert(Set<Card> cards, Card card) {
        if(!cards.contains(card)) {
            LOGGER.debug("Insertamos la carta: {}", card);
            cards.add(card);
        } else {
            LOGGER.debug("La carta {} ya está insertada.", card);
        }
    }

    public static void main(String[] args) {
        Pattern pattern = Pattern.compile("\\[([0-9]+)\\]");
        Matcher matcher = pattern.matcher("Maria2");

        matcher.group();
        Set<Card> cards = new HashSet<>();

        Card[] cards2Insert = {
                new Card(Card.Suit.CLUB, Card.Rank.ACE),
                new Card(Card.Suit.CLUB, Card.Rank.TWO),
                new Card(Card.Suit.CLUB, Card.Rank.THREE),
                new Card(Card.Suit.CLUB, Card.Rank.ACE),
                new Card(Card.Suit.CLUB, null),
        };


        for(Card card: cards2Insert) {
            insert(cards, card);
        }
    }
}
