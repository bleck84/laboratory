# README #

This is my own research's repository. If I try to learn any programming language, write some examples or crazy ideas, I'll update my laboratory repository.

### What is this repository for? ###

Basically, laboratory has the following projects now:

* **bieber-tweets:** It's a Twitter's Streaming's API's example for receiving and filtering tweets relating from Justin Bieber. I've done it in Java 8 with collections and lambdas and you can execute in a Docker Contariner(Instructions for executing is also containing in the project).
* **pocker:** (In construction) It's a Pocker Game based in Java 8. It's an example of functionality in Java 8, with all the new functionalities that Java 8 added for programming.

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* If you want to contacting me, you can write me an email at josejavier.blecua@gmail.com, and I'll try to help you!

Enjoy it!