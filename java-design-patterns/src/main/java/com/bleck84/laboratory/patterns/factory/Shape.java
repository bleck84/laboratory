package com.bleck84.laboratory.patterns.factory;

/**
 * Created by jjblecua on 10/05/2016.
 */
public interface Shape {

    void draw();
}
