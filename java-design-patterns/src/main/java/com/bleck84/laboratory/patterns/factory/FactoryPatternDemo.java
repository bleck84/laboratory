package com.bleck84.laboratory.patterns.factory;

/**
 * Created by jjblecua on 10/05/2016.
 */
public class FactoryPatternDemo {

    private static final String KEY_CIRCLE = "CIRCLE";
    private static final String KEY_RECTANGLE = "RECTANGLE";
    private static final String KEY_SQUARE = "SQUARE";

    public static void main(String[] args) {
        ShapeFactory shapeFactory = new ShapeFactory();

        Shape circle = shapeFactory.getShape(KEY_CIRCLE);
        circle.draw();

        Shape rectangle = shapeFactory.getShape(KEY_RECTANGLE);
        rectangle.draw();

        Shape square = shapeFactory.getShape(KEY_SQUARE);
        square.draw();
    }
}
