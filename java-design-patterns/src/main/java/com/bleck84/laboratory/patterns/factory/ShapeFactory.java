package com.bleck84.laboratory.patterns.factory;

import com.bleck84.laboratory.patterns.factory.impl.Circle;
import com.bleck84.laboratory.patterns.factory.impl.Rectangle;
import com.bleck84.laboratory.patterns.factory.impl.Square;

/**
 * Created by jjblecua on 10/05/2016.
 */
public class ShapeFactory {

    public Shape getShape(String shapeType) {

        Shape shape = null;

        if (shapeType.equals("CIRCLE")) {
            shape = new Circle();
        } else if (shapeType.equals("RECTANGLE")) {
            shape = new Rectangle();
        } else if (shapeType.equals("SQUARE")) {
            shape = new Square();
        }

        return shape;
    }
}
