package com.bleck84.laboratory.patterns.factory.impl;

import com.bleck84.laboratory.patterns.factory.Shape;

/**
 * Created by jjblecua on 10/05/2016.
 */
public class Rectangle implements Shape {

    public void draw() {
        System.out.println("Inside Rectangle::draw() method.");
    }
}
