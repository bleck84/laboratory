package com.bleck84.laboratory.patterns.factory.impl;

import com.bleck84.laboratory.patterns.factory.Shape;

/**
 * Created by jjblecua on 10/05/2016.
 */
public class Square implements Shape {

    public void draw() {
        System.out.println("Inside Square::draw() method. ");
    }
}
